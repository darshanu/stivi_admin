import { Tenant } from './models/tenants';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { Injectable, OnInit } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Building } from './models/buildings';
import { User } from './models/users';
import { Visitor } from './models/visitor';
import { Employee } from './models/employee';

@Injectable({
  providedIn: 'root'
})

export class FirestoreDataService implements OnInit {
  buildingList: Observable<Building[]>;
  tenantList: Observable<Tenant[]>;
  employeeList: Observable<Employee[]>;
  dateList: any;
  visitorList: Observable<Visitor[]>;
  users: any;
  tenantName: any;
  employeeName: any;
  constructor(private afs: AngularFirestore, private db: AngularFireDatabase, private _loadingBar: SlimLoadingBarService) { }

  ngOnInit() {
    this.users = [];
  }

  checkIsAdmin(email): Observable<any> {
    this.users = this.afs.collection('users', ref => ref.where('userEmail', '==', email))
      .snapshotChanges().map(actions => {
        return actions.map(action => {
          const data = action.payload.doc.data() as User;
          const id = action.payload.doc.id;
          return { id, ...data };
        });
      });
    return this.users;
  }

  registerUser(userId, userName, userRole, userEmail, userMobile, userPassword): string {
    const result = 'error';
    this.afs.collection('users')
      .add({
        'userId': userId,
        'userName': userName,
        'userRole': userRole,
        'userEmail': userEmail,
        'userMobile': userMobile,
        'userPassword': userPassword,
        'userIsDeleted': 'N',
        'userCreateDate': new Date().getTime(),
        'userCreateBy': "System"
      })
      .then(docRef => {
        console.log('docRef', docRef);
        return 'success';
      }).catch(error => {
        console.log('error', error);
        return 'error';
      });
    return result;
  }

  storeUserNameInLocalStorageFromEmail(email): Observable<any> {
    console.log(email);
    this.users = this.afs.collection('users', ref => ref.where('userId', '==', email))
      .snapshotChanges()
      .map(actions => {
        return actions.map(action => {
          const data = action.payload.doc.data() as User;
          console.log('data', data);
          const id = action.payload.doc.id;
          return { id, ...data };
        });
      });
    console.log("Users : " + this.users);
    return this.users;
  }

  addServicesToVariant(subServiceList, selectedVehicleType, selectedBrand, selectedModel, variantname): void {
    let result: any;
    this.afs.collection('vehicle')
      .doc(selectedVehicleType).collection('brand')
      .doc(selectedBrand).collection('model')
      .doc(selectedModel).collection('variant')
      .add({
        'services': subServiceList,
        'variantname': variantname
      })
      .then(docRef => {
        result = 'success';
      }).catch(error => {
        result = 'Something went wrong';
      });
  }

  updateServiceToVariant(subServiceList, selectedVehicleType, selectedBrand, selectedModel, selectedVariant, variantname): void {
    let result: any;
    this.afs.collection('vehicle')
      .doc(selectedVehicleType).collection('brand')
      .doc(selectedBrand).collection('model')
      .doc(selectedModel).collection('variant')
      .doc(selectedVariant)
      .set({
        'services': subServiceList,
        'variantname': variantname
      })
      .then(docRef => {
        result = 'success';
      }).catch(error => {
        result = 'Something went wrong';
      });
  }

  getAllUsersList(): Observable<any> {
    this.users = this.afs.collection('users').snapshotChanges()
      .map(actions => {
        return actions.map(action => {
          const data = action.payload.doc.data() as User;
          const id = action.payload.doc.id;
          return { id, ...data };
        });
      });
    return this.users;
  }

  getAllDateList(): Observable<any> {
    console.log('In getAllDateList!');
    this.dateList = this.afs.collection('visitors').snapshotChanges()
      .map(actions => {
        console.log('in map1');
        return actions.map(a => {
          console.log('in map2');
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      });
    console.log('Exit getAllDateList!');
    return this.dateList;
  }

  updateGroup(group, groupId) {
    let result: any;
    console.log(group);
    this.afs.collection('groups').doc(groupId)
      .set({
        groupCreateBy: group.groupCreateBy,
        groupCreateDate: group.groupCreateDate,
        groupId: group.groupId,
        groupIsDeleted: group.groupIsDeleted,
        groupName: group.groupName,
        groupUpdateBy: group.groupUpdateBy,
        groupUpdateDate: group.groupUpdateDate
      }).then(docRef => {
        result = 'success';
      }).catch(error => {
        result = 'Something went wrong';
      });
  }

  getAllGroupList(): Observable<any> {
    this.users = this.afs.collection('groups').snapshotChanges()
      .map(actions => {
        return actions.map(action => {
          const data = action.payload.doc.data() as User;
          const id = action.payload.doc.id;
          return { id, ...data };
        });
      });
    return this.users;
  }

  getAllBuildingList(selectedGroup): Observable<any> {
    this.buildingList = this.afs.collection('groups')
      .doc(selectedGroup).collection('buildings')
      .snapshotChanges()
      .map(actions => {
        return actions.map(action => {
          const data = action.payload.doc.data() as Building;
          const id = action.payload.doc.id;
          return { id, ...data };
        });
      });
    return this.buildingList;
  }

  getAllTenantList(selectedGroup, selectedBuilding): Observable<any> {
    this.tenantList = this.afs.collection('groups')
      .doc(selectedGroup).collection('buildings')
      .doc(selectedBuilding).collection('tenants')
      .snapshotChanges()
      .map(actions => {
        return actions.map(action => {
          const data = action.payload.doc.data() as Tenant;
          const id = action.payload.doc.id;
          return { id, ...data };
        });
      });
    return this.tenantList;
  }
  getAllEmployeeList(selectedGroup, selectedBuilding, selectedTenant): Observable<any> {
    console.log('selectedGroup : ' + selectedGroup + ' selectedBuilding : ' + selectedBuilding + ' selectedTenant : ' + selectedTenant);
    this.employeeList = this.afs.collection('groups')
      .doc(selectedGroup).collection('buildings')
      .doc(selectedBuilding).collection('tenants')
      .doc(selectedTenant).collection('employee')
      .snapshotChanges()
      .map(actions => {
        return actions.map(action => {
          const data = action.payload.doc.data() as Employee;
          const id = action.payload.doc.id;
          return { id, ...data };
        });
      });
    return this.employeeList;
  }

  getTenantNameById(selectedGroup, selectedBuilding, selectedTenant) {
    var docRef = this.afs.collection('groups').doc(selectedGroup)
      .collection('buildings').doc(selectedBuilding)
      .collection('tenants').doc(selectedTenant).ref;
    var getDoc = docRef.get()
      .then(doc => {
        if (!doc.exists) {
          console.log('No such document!');
        } else {
          console.log('Document data:', doc.data().tenantName);
          this.tenantName = doc.data().tenantName;
          return doc.data().tenantName;
        }
      })
      .catch(err => {
        console.log('Error getting document', err);
      });
  }

  getEmployeeNameById(selectedGroup, selectedBuilding, selectedTenant, selectedEmployee) {
    var docRef = this.afs.collection('groups').doc(selectedGroup)
      .collection('buildings').doc(selectedBuilding)
      .collection('tenants').doc(selectedTenant)
      .collection('employees').doc(selectedEmployee).ref;
    var getDoc = docRef.get()
      .then(doc => {
        if (!doc.exists) {
          console.log('No such document!');
        } else {
          console.log('Document data:', doc.data().employeeName);
          this.employeeName = doc.data().employeeName;
          return doc.data().employeeName;
        }
      })
      .catch(err => {
        console.log('Error getting document', err);
      });
  }

  getAllVisitorList(selectedGroup, selectedBuilding, tenantName, selectedDate): Observable<any> {
    console.log(tenantName);
    console.log('RS-->' + selectedDate);
    if (tenantName == 'All') {
      this.visitorList = this.afs.collection('visitors')
        .doc(selectedDate).collection('records', ref => ref.where('visitGroupId', '==', selectedGroup)
        .where('visitBuildingId', '==', selectedBuilding))
        .snapshotChanges()
        .map(actions => {
          return actions.map(action => {
            const data = action.payload.doc.data() as Visitor;
            const id = action.payload.doc.id;
            return { id, ...data };
          });
        });
    } else {
      this.visitorList = this.afs.collection('visitors')
        .doc(selectedDate).collection('records', ref => ref.where('meetingTenantName', '==', this.tenantName))
        .snapshotChanges()
        .map(actions => {
          return actions.map(action => {
            const data = action.payload.doc.data() as Visitor;
            const id = action.payload.doc.id;
            return { id, ...data };
          });
        });
    }
    return this.visitorList;
  }

  addNewGroup(group) {
    let result: any;
    this.afs.collection('groups')
      .add({
        groupCreateBy: group.groupCreateBy,
        groupCreateDate: group.groupCreateDate,
        groupId: group.groupId,
        groupIsDeleted: group.groupIsDeleted,
        groupName: group.groupName
      })
      .then(docRef => {
        result = 'success';
      }).catch(error => {
        result = 'Something went wrong';
      });
  }

  addNewBuilding(selectedGroup, building) {
    let result: any;
    this.afs.collection('groups')
      .doc(selectedGroup).collection('buildings')
      .add({
        buildingCreateBy: building.buildingCreateBy,
        buildingCreateDate: building.buildingCreateDate,
        buildingIsDeleted: 'N',
        buildingName: building.buildingName,
        buildingWorkHours: building.buildingWorkHours,
        buildingLocation: building.buildingLocation
      })
      .then(docRef => {
        result = 'success';
      }).catch(error => {
        result = 'Something went wrong';
      });
  }

  addNewTenant(selectedGroup, selectedBuilding, tenant) {
    let result: any;
    this.afs.collection('groups')
      .doc(selectedGroup).collection('buildings')
      .doc(selectedBuilding).collection('tenants')
      .add({
        tenantCreateBy: tenant.tenantCreateBy,
        tenantCreateDate: tenant.tenantCreateDate,
        tenantIsDeleted: tenant.tenantIsDeleted,
        tenantName: tenant.tenantName,
        tenantUnitNo: tenant.tenantUnitNo,
        tenantStatus: tenant.tenantStatus,
        tenantVisitType: tenant.tenantVisitType,
        tenantWorkHours: tenant.tenantWorkHours
      })
      .then(docRef => {
        result = 'success';
      }).catch(error => {
        result = 'Something went wrong';
      });
  }

  addNewEmployee(selectedGroup, selectedBuilding, selectedTenant, employees) {
    let result: any;
    this.afs.collection('groups')
      .doc(selectedGroup).collection('buildings')
      .doc(selectedBuilding).collection('tenants')
      .doc(selectedTenant).collection('employee')
      .add({
        empName: employees.empName,
        empEmail: employees.empEmail,
        empContact: employees.empContact,
        empNotification: employees.empNotification,
        empCreateBy: employees.empCreateBy,
        empCreateDate: employees.empCreateDate,
        empIsDeleted: employees.empIsDeleted

      })
      .then(docRef => {
        result = 'success';
      }).catch(error => {
        result = 'Something went wrong';
      });
  }

  updateUser(user) {
    let result: any;
    console.log(user);
    this.afs.collection('users').doc(user.id)
      .set({
        userId: user.userId,
        userName: user.userName,
        userEmail: user.userEmail,
        userMobile: user.userMobile,
        userRole: user.userRole,
        userCreateBy: user.userCreateBy,
        userCreateDate: user.userCreateDate,
        userUpdateBy: user.userUpdateBy,
        userUpdateDate: user.userUpdateDate,
        userIsDeleted: user.userIsDeleted,
        userModifiedAt: new Date().getTime()
      }).then(docRef => {
        result = 'success';
      }).catch(error => {
        result = 'Something went wrong';
      });
  }

  updateBuilding(selectedGroup, building, buildingId) {
    console.log(building);
    let result: any;
    this.afs.collection('groups')
      .doc(selectedGroup).collection('buildings')
      .doc(buildingId)
      .set({
        buildingCreateBy: building.buildingCreateBy,
        buildingCreateDate: building.buildingCreateDate,
        buildingIsDeleted: building.buildingIsDeleted,
        buildingName: building.buildingName,
        buildingUpdateBy: building.buildingUpdateBy,
        buildingUpdateDate: building.buildingUpdateDate,
        buildingWorkHours: building.buildingWorkHours
      }).then(docRef => {
        result = 'success';
      }).catch(error => {
        result = 'Something went wrong';
      });
  }

  updateTenant(selectedGroup, selectedBuilding, tenant, tenantId) {
    let result: any;
    this.afs.collection('groups')
      .doc(selectedGroup).collection('buildings')
      .doc(selectedBuilding).collection('tenants').doc(tenantId)
      .set({
        tenantCreateBy: tenant.tenantCreateBy,
        tenantCreateDate: tenant.tenantCreateDate,
        tenantIsDeleted: tenant.tenantIsDeleted,
        tenantName: tenant.tenantName,
        tenantUnitNo: tenant.tenantUnitNo,
        tenantStatus: tenant.tenantStatus,
        tenantVisitType: tenant.tenantVisitType,
        tenantWorkHours: tenant.tenantWorkHours,
        tenantUpdateBy: tenant.tenantUpdateBy,
        tenantUpdateDate: tenant.tenantUpdateDate
      }).then(docRef => {
        result = 'success';
      }).catch(error => {
        result = 'Something went wrong';
      });
  }

  updateEmployee(selectedGroup, selectedBuilding, selectedTenant, employees, employeeId) {
    let result: any;
    this.afs.collection('groups')
      .doc(selectedGroup).collection('buildings')
      .doc(selectedBuilding).collection('tenants')
      .doc(selectedTenant).collection('employee').doc(employeeId)
      .set({
        empName: employees.empName,
        empEmail: employees.empEmail,
        empContact: employees.empContact,
        empNotification: employees.empNotification,
        empCreateBy: employees.empCreateBy,
        empCreateDate: employees.empCreateDate,
        empIsDeleted: employees.empIsDeleted,
        empUpdateBy: employees.empUpdateBy,
        empUpdateDate: employees.empUpdateDate
      }).then(docRef => {
        result = 'success';
      }).catch(error => {
        result = 'Something went wrong';
      });
  }

  removeGroup(groupId) {
    let result: any;
    this.afs.collection('groups')
      .doc(groupId).delete().then(docRef => {
        result = 'success';
      }).catch(error => {
        result = 'Something went wrong';
      });
  }

  removeUser(userId) {
    let result: any;
    this.afs.collection('users')
      .doc(userId).delete().then(docRef => {
        result = 'success';
      }).catch(error => {
        result = 'Something went wrong';
      });
  }

  removeBuilding(selectedGroup, selectedBuilding, buildingId) {
    let result: any;
    this.afs.collection('groups')
      .doc(selectedGroup).collection('buildings')
      .doc(selectedBuilding).delete().then(docRef => {
        result = 'success';
      }).catch(error => {
        result = 'Something went wrong';
      });
  }

  removeTenant(selectedGroup, selectedBuilding, tenantId) {
    let result: any;
    this.afs.collection('groups')
      .doc(selectedGroup).collection('buildings')
      .doc(selectedBuilding).collection('tenants').doc(tenantId)
      .delete().then(docRef => {
        result = 'success';
      }).catch(error => {
        result = 'Something went wrong';
      });
  }
  removeEmployee(selectedGroup, selectedBuilding, selectedTenant, employeeId) {
    let result: any;
    this.afs.collection('groups')
      .doc(selectedGroup).collection('buildings')
      .doc(selectedBuilding).collection('tenants')
      .doc(selectedTenant).collection('employee').doc(employeeId)
      .delete().then(docRef => {
        result = 'success';
      }).catch(error => {
        result = 'Something went wrong';
      });
  }

}
