import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VisitorRoutingModule } from './visitor-routing.module';
import { ListVisitorComponent } from './list-visitor/list-visitor.component';

@NgModule({
  imports: [
    CommonModule,
    VisitorRoutingModule,
    FormsModule,
    NgbModule.forRoot()
  ],
  declarations: [ListVisitorComponent]
})
export class VisitorModule { }
