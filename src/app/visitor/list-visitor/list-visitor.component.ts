import { Tenant } from './../../models/tenants';
import { Building } from './../../models/buildings';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FirestoreDataService } from './../../firestore-data.service';
import { Group } from './../../models/group';
import { Visitor } from './../../models/visitor';

import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import * as dateFormat from 'dateformat';
import { from } from 'rxjs';

@Component({
  selector: 'app-list-visitor',
  templateUrl: './list-visitor.component.html',
  styleUrls: ['./list-visitor.component.scss']
})
export class ListVisitorComponent implements OnInit {
  groupList: Group[];
  buildingList: Building[];
  tenantList: Tenant[];
  tenantAll: Tenant;
  dateList: any[];
  visitorList: Visitor[];
  isGroupListEmpty: boolean;
  isBuidlingListEmpty: boolean;
  isTenantListEmpty: boolean;
  isdateListEmpty: boolean;
  isVisitorListEmpty: boolean;
  isUpdate: boolean;
  ngModelRef: any;
  selectedBuilding: string;
  selectedGroup: string;
  selectedTenant: string;
  selectedDate: string;
  selectedVisitor: string;
  group: Group;
  building: Building;
  tenant: Tenant;
  visitor: Visitor;
  
  buildingId: any;
  groupId: any;
  tenantId: any;
  selectedBuildingDeleted: any;
  selectedTenantDeleted: any;
  selectedGroupDeleted: any;
  selectedTenantStatus: any;
  selectedTenantVsitType: any;
  tenantName: any;




  constructor(private _firestoreDataService: FirestoreDataService, private modalService: NgbModal,
    private router: Router) { }

  ngOnInit() {
    console.log('Test...');
    this.selectedBuildingDeleted = 'N';
    this.selectedTenantDeleted = 'N';
    this.selectedGroupDeleted = 'N';
    this.selectedTenantStatus = 'Active';
    this.selectedGroup = 'lYDZRANkMlDMwYUb1DAe';
    this.isBuidlingListEmpty = false;
    this.isGroupListEmpty = false;
    this.isTenantListEmpty = false;
    this.group = new Group();
    this.building = new Building();
    this.tenant = new Tenant();
    this.groupList = [];
    this.isUpdate = false;
    this.selectedDate = moment().format('DD-MM-YYYY');
    

    this.tenantName = 'All';
    this._firestoreDataService.getAllGroupList().subscribe(data => {
      this.groupList = data;
      if (this.groupList.length !== 0) {
        this.isGroupListEmpty = false;
        this.selectedGroup = this.groupList[0].id;
        console.log('group', this.groupList);
        this.groupList.forEach(group => {
          group.groupCreateDate = new Date(Number(group.groupCreateDate)).toLocaleString();
          group.groupUpdateDate = new Date(Number(group.groupUpdateDate)).toLocaleString();
        });
        this.getAllBuildingList();
      } else {
        this.isGroupListEmpty = true;
        this.buildingList = [];
      }
    });
  }
  getSelectedDate() {
    let d = new Date(this.selectedDate);
    let datestring =   ('0' + d.getDate()).slice(-2) + '-' + ('0' + (d.getMonth() + 1)).slice(-2) + '-' + d.getFullYear();
    console.log(datestring);
    return datestring;
   
  }
  getAllBuildingList() {
    this.buildingList = [];
    this.tenantList = [];
    this._firestoreDataService.getAllBuildingList(this.selectedGroup).subscribe(data => {
      console.log('building', data);
      this.buildingList = data;
      if (this.buildingList.length !== 0) {
        this.isBuidlingListEmpty = false;
        this.selectedBuilding = this.buildingList[0].id;
        this.buildingList.forEach(building => {
          if (building.buildingUpdateDate) {
            building.buildingUpdateDate = new Date(Number(building.buildingUpdateDate)).toLocaleString();
          }
          building.buildingCreateDate = new Date(Number(building.buildingCreateDate)).toLocaleString();
        });
        this.getAllTenantList();
      } else {
        this.isBuidlingListEmpty = true;
      }
    });
  }

  getTenantNameById() {
    if (this.selectedTenant == 'all')
      this.tenantName = 'All';
    else
      this.tenantName = this._firestoreDataService.getTenantNameById(this.selectedGroup, this.selectedBuilding, this.selectedTenant);
  }

  getAllTenantList() {
    this.tenantList = [];
    this._firestoreDataService.getAllTenantList(this.selectedGroup, this.selectedBuilding).subscribe(data => {
      this.tenantList = data;
      console.log(this.tenantList);
      if (this.tenantList.length !== 0) {
        this.isTenantListEmpty = false;
        this.selectedTenant = 'all';
        this.tenantList.forEach(tenant => {
          tenant.tenantCreateDate = new Date(Number(tenant.tenantCreateDate)).toLocaleString();
          if (tenant.tenantUpdateDate) {
            tenant.tenantUpdateDate = new Date(Number(tenant.tenantUpdateDate)).toLocaleString();
          }
        });
        this.getAllVisitorList();
      } else {
        this.isTenantListEmpty = true;
      }
    });
  }

  getAllVisitorList() {
    this.getSelectedDate();
    this.visitorList = [];
    this._firestoreDataService.getAllVisitorList(this.selectedGroup, this.selectedBuilding, this.tenantName, this.getSelectedDate()).subscribe(data => {
      this.visitorList = data;
      console.log(this.visitorList)
      console.log('visitorList : ' + this.visitorList + 'selected date :' + this.getSelectedDate());
      if (this.visitorList.length !== 0) {
        this.isVisitorListEmpty = false;
        this.selectedVisitor = this.visitorList[0].id;
        this.visitorList.sort(function (a, b) {
          return Number(b.visitRequestTime) - Number(a.visitRequestTime);
        });
        this.visitorList.forEach(visitor => {
          visitor.visitRequestTime = new Date(Number(visitor.visitRequestTime)).toLocaleString();
        });
      } else {
        this.isVisitorListEmpty = true;
      }
    });
  }

  public getRowsValue(flag) {
    if (flag === null) {
      return this.visitorList.length;
    }
  }

  openModal(content) {
    this.building = new Building();
    this.group = new Group();
    this.tenant = new Tenant();
    this.visitor = new Visitor();
    this.isUpdate = false;
    this.ngModelRef = this.modalService.open(content, { centered: true });
  }

  closeModal() {
    this.ngModelRef.close();
    this.isUpdate = false;
    this.group = null;
    this.building = null;
    this.tenant = null;
    this.visitor = null;
  }

  addNewTenant() {
    this.tenant.tenantCreateDate = new Date().getTime().toString();
    this.tenant.tenantIsDeleted = this.selectedTenantDeleted === 'Y' ? true : false;
    this.tenant.tenantStatus = this.selectedTenantStatus;
    this.tenant.tenantCreateBy = localStorage.getItem('username');
    console.log(this.tenant);
    this._firestoreDataService.addNewTenant(this.selectedGroup, this.selectedBuilding, this.tenant);
    this.ngModelRef.close();
    this.tenant = null;
  }

  updateTenant() {
    this.tenant.tenantCreateDate = Date.parse(this.tenant.tenantCreateDate).toString();
    this.tenant.tenantUpdateDate = new Date().getTime().toString();
    this.tenant.tenantUpdateBy = localStorage.getItem('username');
    this.tenant.tenantIsDeleted = this.selectedTenantDeleted === 'Y' ? true : false;
    console.log(this.tenant);
    this._firestoreDataService.updateTenant(this.selectedGroup, this.selectedBuilding, this.tenant, this.tenantId);
    this.ngModelRef.close();
    this.tenant = null;
    this.tenantId = null;
    this.isUpdate = false;
  }

  editTenant(tenantDetails, content) {
    console.log(tenantDetails);
    this.tenant = tenantDetails;
    this.tenantId = tenantDetails.id;
    this.ngModelRef = this.modalService.open(content, { centered: true });
    this.isUpdate = true;
  }

  removeTenant(tenantDetails) {
    if (confirm('Are you sure to remove tenant')) {
      this._firestoreDataService.removeTenant(this.selectedGroup, this.selectedBuilding, tenantDetails.id);
    }
  }
}
