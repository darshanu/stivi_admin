import { ListVisitorComponent } from './list-visitor/list-visitor.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Visitor Master'
    },
    children: [
      {
        path: 'list',
        component: ListVisitorComponent,
        data: {
          title: 'List Visitor'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VisitorRoutingModule { }
