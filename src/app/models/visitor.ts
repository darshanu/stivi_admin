export class Visitor {
    id: string;
    comingFrom: string;
    meetingPersonName: string;
    meetingPurpose: string;
    meetingTenantId: string;
    meetingTenantUnitNo: string;
    meetingTenantName: string;
    visitBuildingId: string;
    visitGroupId: string;
    visitId: string;
    visitRequestTime: string;
    visitorContactNo: string;
    visitorEmailID: string;
    visitorIdUrl: string;
    visitorName: string;
    visitorOtp: number;
    visitorPhotoUrl: string;
}
