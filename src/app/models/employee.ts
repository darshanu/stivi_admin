export class Employee {
    id: string;
    
    empName: string;
    empEmail: string;
    empContact: string;
    empNotification: boolean;
    
    empCreateBy: string;
    empCreateDate: string;
    
    empIsDeleted: boolean;
    empUpdateBy: string;
    empUpdateDate: string;
    
  }