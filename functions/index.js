const functions = require('firebase-functions');
const admin = require('firebase-admin');
//const axios = require('axios');
const http = require('http');
admin.initializeApp();

exports.addMessage = functions.https.onRequest((req, res) => {
    const original = req.query.text;
    return admin.firestore().collection('messages').add({original: original}).then((writeResult) => {
      return res.json({result: `Message with ID: ${writeResult.id} added.`});
    });
  });

exports.makeUppercase = functions.firestore.document('/messages/{documentId}').onCreate((snap, context) => {
  const original = snap.data().original;
  console.log('Uppercasing', context.params.documentId, original);
  const uppercase = original.toUpperCase();
  console.log(snap.ref.id);
  return snap.ref.set({uppercase}, {merge: true});
});

exports.sendHttpRequest = functions.firestore.document('/messages/{documentId}').onCreate((snap, context) => {
  axios.get('https://api.nasa.gov/planetary/apod?api_key=DEMO_KE')
  .then(response => {
    console.log(response.data.url);
    console.log(response.data.explanation);
    return 1;//Promise.resolve(response.data.explanation);
  })
  .catch(error => {
    console.log(error);
    return 2;//Promise.reject(error);
  });
});

/*
exports.sendVisitorOtp = functions.firestore.document('/visitors/{date}/records/{visitId}').onCreate((snap, context) => {
  console.log('Sending OTP for refid : ' + snap.ref.id + '\n Visitor Name : ' + snap.data().visitorFirstName + ' ' + snap.data().visitorLastName);
  const visitorContactNo = admin.firestore().collection('visitors').get.visitorContactNo;
  const meetingTenantName = snap.data().meetingTenantName;
  const otp = Math.floor(Math.random() * (9999 - 1001)) + 1001; 
  const mesg = 'Welcome to ' +meetingTenantName+ '! Your OTP to get the visitor pass is ' +otp;
  axios.get('http://sms.bulkssms.com/submitsms.jsp?user=STEIGEN&key=7c0141340bXX&mobile='+visitorContactNo+'&message='+mesg+'&senderid=STIVIS&accusage=1')
  .then(response => {
    console.log(response);
    return Promise.resolve(response);
    //response : sent,success,2074060,2134905199,+919773600798
  })
  .catch(error => {
    console.log(error);
    return Promise.reject(error);
  });
});
*/

function getVisitDetails(visitDate,visitId) {
  var visitorRef = admin.firestore().collection('visitors').doc(visitDate).collection('records').doc(visitId);
  console.log(visitorRef);
  var visit = visitorRef.get()
    .then(doc => {
      if (!doc.exists) {
        console.log('Visit Information Not Found!');
        return {};
      } else {
        console.log('Visit Detail :', doc.data());
        console.log('Visit Contact No :', doc.data().visitorContactNo);
        return doc.data();
      }      
    }) 
    .catch(err => {
      console.log('Error getting visit details', err);
      return {};
    });
}

exports.sendVisitorOtpHttp = functions.https.onRequest((req, res) => {
  var visitorRef = admin.firestore().collection('visitors').doc(req.query.visitDate).collection('records').doc(req.query.visitId);
  var visit = visitorRef.get()
    .then(doc => {
      if (!doc.exists) {
        console.log('Visit Information Not Found!');
        return res.status(404).json({result: 'Visit Information Not Found!'});
      } else {
        var visitDetails = doc.data();
        console.log('Visit Detail :', visitDetails);
        var visitorContactNo = visitDetails.visitorContactNo;
        var meetingTenantName = visitDetails.meetingTenantName;
        const otp = Math.floor(Math.random() * (9999 - 1001)) + 1001; 
        //const mesg = 'Welcome to ' +meetingTenantName+ '! Your OTP to get the visitor pass is ' +otp;
        const mesg = 'Welcome to ' +meetingTenantName+ '\nVisitor: '+visitDetails.visitorName+ '\nComing From: '+visitDetails.comingFrom+ '\nMeeting: '+visitDetails.meetingPersonName+ '\nPurpose: '+visitDetails.meetingPurpose+ '\nYour OTP to print pass is '+otp;
        http.get('http://sms.bulkssms.com/submitsms.jsp?user=STEIGEN&key=7c0141340bXX&mobile='+visitorContactNo+'&message='+encodeURIComponent(mesg)+'&senderid=STIVIS&accusage=1', (resp) => {
          let data = '';
          resp.on('data', (chunk) => {
            data += chunk;
          });  
          resp.on('data', () => {
            data = data.trim()
            if(data.indexOf('sent')>=0){
              data = data + ',' + otp;
            }
            visitorRef.set({visitorOtp:otp}, {merge: true});
            console.log("response received : "+ data);
            return res.status(200).json({result:data});
          });
        }).on("error", (err) => {
          console.log("Error occured : " + err.message);
          return res.status(500).json({result:error.message});
        });
        return res;
      }
    })
    .catch(err => {
      console.log('Error getting visit details', err);
      return res.status(500).json({result:err});
    });
});

exports.tester = functions.https.onRequest((req, res) => {
  //https://us-central1-stivi-development.cloudfunctions.net/sendVisitorOtpHttp?visitorContactNo=9773600798&visitorFirstName=Darshan&visitorLastName=Upadhyay&meetingTenantName=Steigensoft
  //https://us-central1-stivi-development.cloudfunctions.net/tester?visitDate=09-12-2018&visitId=Cqzrl5rQFn7wPZtDqpy5
  console.log('Sending OTP for VisitId : ' + req.query.visitId + ' Date : ' + req.query.visitDate + ' Attempt :' + req.query.attempt);
  var visitDetails = getVisitDetails(req.query.visitDate,req.query.visitId);
  const visitorContactNo = visitDetails.visitorContactNo; //admin.firestore().collection('visitors').doc().visitorContactNo;
  const meetingTenantName = visitDetails.meetingTenantName;//req.query.meetingTenantName;
  const otp = Math.floor(Math.random() * (9999 - 1001)) + 1001; 
  const mesg = 'Welcome to ' +meetingTenantName+ '! Your OTP to get the visitor pass is ' +otp;
  http.get('http://sms.bulkssms.com/submitsms.jsp?user=STEIGEN&key=7c0141340bXX&mobile='+visitorContactNo+'&message='+mesg+'&senderid=STIVIS&accusage=1', (resp) => {
    let data = '';

    resp.on('data', (chunk) => {
      data += chunk;
    });

    resp.on('data', () => {
      console.log("response received : "+ data);
      return res.status(200).send(data);
    });
  }).on("error", (err) => {
    console.log("Error occured : " + err.message);
    return res.status(500).send(error.message);
    
  });
});